//
//  NotificationNameExtension.swift
//  Notifications and Observers
//
//  Created by Florentin Lupascu on 23/04/2019.
//  Copyright © 2019 Florentin. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let receiveText = Notification.Name(rawValue: "receiveTextNotification")
    static let lightTheme = Notification.Name(rawValue: "themeSelected.light")
    static let darkTheme = Notification.Name(rawValue: "themeSelected.dark")
}
