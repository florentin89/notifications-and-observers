//
//  ViewController.swift
//  Notifications and Observers
//
//  Created by Florentin on 20/07/2018.
//  Copyright © 2018 Florentin. All rights reserved.
//

import UIKit

class FirstVC: UIViewController {
    
    // Interface Links
    @IBOutlet weak var titlePageLabel: UILabel!
    @IBOutlet weak var changeThemeBtnOutlet: UIButton!
    @IBOutlet weak var label: UILabel!
    
    // Remove observer from memory after we use it
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // Life Cycles
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Customize ChangeTheme Btn
        changeThemeBtnOutlet.layer.cornerRadius = changeThemeBtnOutlet.frame.size.height/2
        createObservers()
    }
    
    // Function to create the observers
    func createObservers(){
        // Light Theme
        NotificationCenter.default.addObserver(self, selector: #selector(FirstVC.updateTitleColor(_:)), name: .lightTheme, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(FirstVC.updateBackground(_:)), name: .lightTheme, object: nil)
        
        // Dark Theme
        NotificationCenter.default.addObserver(self, selector: #selector(FirstVC.updateTitleColor(_:)), name: .darkTheme, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(FirstVC.updateBackground(_:)), name: .darkTheme, object: nil)
        
        // Text Received
        NotificationCenter.default.addObserver(self, selector: #selector(getText), name: .receiveText, object: nil)
    }
    
    @objc func updateTitleColor(_ notification: NSNotification){
        let isLightTheme = notification.name == .lightTheme
        isLightTheme ? (titlePageLabel.textColor = .black) : (titlePageLabel.textColor = .white)
    }
    
    @objc func updateBackground(_ notification: NSNotification){
        let isLightTheme = notification.name == .lightTheme
        let color = isLightTheme ? UIColor.white : UIColor.black
        view.backgroundColor = color
    }
    
    @objc func getText(_ notification: Notification) {
        if let object = notification.object as? [String: Any] {
            if let textReceived = object["text"] as? String {
                print(textReceived)
                label.text = textReceived
            }
        }
    }
    
    @IBAction func changeThemeBtn(_ sender: UIButton) {
        let selectionVC = storyboard?.instantiateViewController(withIdentifier: "ThemeSelection") as! SecondVC
        present(selectionVC, animated: true, completion: nil)
    }
}
