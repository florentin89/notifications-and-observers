//
//  SecondViewController.swift
//  Notifications and Observers
//
//  Created by Florentin on 20/07/2018.
//  Copyright © 2018 Florentin. All rights reserved.
//

import UIKit

class SecondVC: UIViewController {
    
    // Interface Links
    @IBOutlet weak var lightThemeBtnOutlet: UIButton!
    @IBOutlet weak var darkThemeBtnOutlet: UIButton!
    @IBOutlet weak var textfield: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Customize Buttons
        lightThemeBtnOutlet.layer.cornerRadius = lightThemeBtnOutlet.frame.size.height/2
        darkThemeBtnOutlet.layer.cornerRadius = darkThemeBtnOutlet.frame.size.height/2
    }
    
    // Change the Profit page theme into Light
    @IBAction func lightThemeButton(_ sender: UIButton) {
        
        sendText()
        NotificationCenter.default.post(name: .lightTheme, object: nil)
        dismiss(animated: true, completion: nil)
    }
    
    // Change the Profit page theme into Dark
    @IBAction func darkThemeButton(_ sender: UIButton) {
        
        sendText()
        NotificationCenter.default.post(name: .darkTheme, object: nil)
        dismiss(animated: true, completion: nil)
    }
    
    func sendText(){
        let object: [String: Any] = ["text": textfield.text ?? String()]
        NotificationCenter.default.post(name: .receiveText, object: object)
    }
}
